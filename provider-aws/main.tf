terraform {
    # Use GitLab Terraform backend.
    backend "http" {
        # The following variables will be set automatically on GitLab CI thanks to the GitLab Terraform image
        # (see https://gitlab.com/gitlab-org/terraform-images/-/blob/master/src/bin/gitlab-terraform.sh)
        # If you want to run Terraform locally from your computer, you'll need to export these variables manually:
        #     TF_HTTP_ADDRESS, TF_HTTP_LOCK_ADDRESS, TF_HTTP_UNLOCK_ADDRESS, TF_HTTP_USERNAME, TF_HTTP_PASSWORD
        # Note that CI_PROJECT_ID is the *numeric* project ID shown on the project's GitLab homepage; it is not the project name/path.

        # address = "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/terraform/state/${TF_STATE_NAME}"
        # lock_address = "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/terraform/state/${TF_STATE_NAME}/lock"
        # unlock_address = "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/terraform/state/${TF_STATE_NAME}/lock"
        lock_method = "POST"
        unlock_method = "DELETE"
        retry_wait_min = 5
    }

    required_providers {
        aws = {
            source  = "hashicorp/aws"
            version = "~> 3.0"
        }
        kubernetes = {
            source  = "hashicorp/kubernetes"
            version = "~> 2.0.2"
        }
    }
}

variable "cluster_name" { type = string }
variable "cluster_max_node_count" { default = 5 }  # Sets the maximum size of the EKS autoscaling group (asg_max_size). High values = $$$$
variable "aws_region" { default = "us-east-1" }

# Configure the AWS Provider
provider "aws" {
    region = var.aws_region
    # Set the access key and secret key via environment variables (AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)
}

data "aws_availability_zones" "available" {}

# Pre-declare data sources that we can use to get the cluster ID and auth info, once it's created
data "aws_eks_cluster" "cluster" { name = module.eks.cluster_id }
data "aws_eks_cluster_auth" "cluster" { name = module.eks.cluster_id }

# The EKS module we use to provision EKS (below) requires that we configure the kubernetes  provider:
provider "kubernetes" {
    host                   = data.aws_eks_cluster.cluster.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
    token                  = data.aws_eks_cluster_auth.cluster.token
}

provider "helm" {
    kubernetes {
        host                   = data.aws_eks_cluster.cluster.endpoint
        cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
        token                  = data.aws_eks_cluster_auth.cluster.token
    }
}

####################################################################################################
## VPC: A virtual private cloud which will hold everything else
####################################################################################################
module "vpc" {
    source = "terraform-aws-modules/vpc/aws"

    name = "${var.cluster_name} VPC"
    cidr = "10.0.0.0/16"

    # "Amazon EKS requires subnets in at least two Availability Zones."
    azs             = data.aws_availability_zones.available.names
    # "We recommend a VPC with public and private subnets so that Kubernetes can create public load balancers in the
    #  public subnets that load balance traffic to pods running on nodes that are in private subnets."
    # https://docs.aws.amazon.com/eks/latest/userguide/network_reqs.html
    private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
    public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

    enable_nat_gateway = true
    single_nat_gateway = true
    enable_vpn_gateway = false

    # "Your VPC must have DNS hostname and DNS resolution support. Otherwise, your nodes cannot register with your
    #  cluster."
    # https://docs.aws.amazon.com/eks/latest/userguide/network_reqs.html
    enable_dns_support = true
    enable_dns_hostnames = true

    tags = {
        ManagedBy = "Tutorraform"
        Terraform = "true"
        "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    }
    public_subnet_tags = {
        "kubernetes.io/cluster/${var.cluster_name}" = "shared"
        "kubernetes.io/role/elb"                      = "1"
    }
    private_subnet_tags = {
        "kubernetes.io/cluster/${var.cluster_name}" = "shared"
        "kubernetes.io/role/internal-elb"             = "1"
    }
}

####################################################################################################
## Security Groups
####################################################################################################

# Allow SSH into the worker nodes from any other host in the VPC (such as a bastion)
resource "aws_security_group" "k8s_worker_node_ssh_access" {
    name_prefix = "${var.cluster_name}_worker_ssh"
    vpc_id      = module.vpc.vpc_id

    ingress {
        from_port = 22
        to_port   = 22
        protocol  = "tcp"

        cidr_blocks = ["10.0.0.0/8"]
    }
    tags = {
        ManagedBy = "Tutorraform"
        Terraform = "true"
    }
}


####################################################################################################
## The Kubernetes Cluster itself
####################################################################################################

module "eks" {
    source             = "terraform-aws-modules/eks/aws"
    cluster_name       = var.cluster_name
    cluster_version    = "1.19"
    subnets            = module.vpc.private_subnets
    vpc_id             = module.vpc.vpc_id
    enable_irsa        = true  # Required for autoscaling
    write_kubeconfig   = false  # Don't write the kubeconfig to a local file this way; doesn't mix well with a remote state backend

    workers_group_defaults = {
        root_volume_type = "gp2"
    }

    worker_groups = [
        {
            name                 = "${var.cluster_name}-workers"
            instance_type        = "t3.medium"  # Tutor recommends at least 4GB per worker node.
            asg_desired_capacity = 2  # Initial value only; changing this will not scale your cluster! See https://github.com/terraform-aws-modules/terraform-aws-eks/blob/master/docs/autoscaling.md
            asg_min_size         = 2  # Smaller than 2 is not going to work, given all the things we're currently deploying for each instance
            asg_max_size         = var.cluster_max_node_count
            tags = [
                {
                    "key"                 = "k8s.io/cluster-autoscaler/enabled"
                    "propagate_at_launch" = "false"
                    "value"               = "true"
                },
                {
                    "key"                 = "k8s.io/cluster-autoscaler/${var.cluster_name}"
                    "propagate_at_launch" = "false"
                    "value"               = "true"
                }
            ]
        },
    ]

    tags = {
        ManagedBy = "Tutorraform"
        Terraform = "true"
    }
}

# Declare the kubeconfig as an output - access it anytime with "/tf output -raw kubeconfig"
output "kubeconfig" {
    value     = module.eks.kubeconfig
    sensitive = true
}

####################################################################################################
## Cluster Autoscaler - this scales the nodes up and down as needed
####################################################################################################

# based on IRSA example, https://github.com/terraform-aws-modules/terraform-aws-eks/tree/master/examples/irsa

locals {
    autoscaler_account_name = "cluster-autoscaler"
}

module "iam_assumable_role_admin" {
    source                        = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
    version                       = "3.13.0"
    create_role                   = true
    role_name                     = "cluster-autoscaler"
    provider_url                  = replace(module.eks.cluster_oidc_issuer_url, "https://", "")
    role_policy_arns              = [aws_iam_policy.cluster_autoscaler.arn]
    oidc_fully_qualified_subjects = ["system:serviceaccount:kube-system:${local.autoscaler_account_name}"]
}

resource "aws_iam_policy" "cluster_autoscaler" {
    name_prefix = "cluster-autoscaler"
    description = "EKS cluster-autoscaler policy for cluster ${module.eks.cluster_id}"
    policy      = data.aws_iam_policy_document.cluster_autoscaler.json
}

# Permissions required for the autoscaler
data "aws_iam_policy_document" "cluster_autoscaler" {
    statement {
        sid     = "clusterAutoscalerAll"
        effect  = "Allow"
        actions = [
            "autoscaling:DescribeAutoScalingGroups",
            "autoscaling:DescribeAutoScalingInstances",
            "autoscaling:DescribeLaunchConfigurations",
            "autoscaling:DescribeTags",
            "ec2:DescribeLaunchTemplateVersions",
        ]

        resources = ["*"]
    }

    statement {
        sid    = "clusterAutoscalerOwn"
        effect = "Allow"

        actions = [
            "autoscaling:SetDesiredCapacity",
            "autoscaling:TerminateInstanceInAutoScalingGroup",
            "autoscaling:UpdateAutoScalingGroup",
        ]

        resources = ["*"]

        condition {
            test     = "StringEquals"
            variable = "autoscaling:ResourceTag/kubernetes.io/cluster/${module.eks.cluster_id}"
            values   = ["owned"]
        }

        condition {
            test     = "StringEquals"
            variable = "autoscaling:ResourceTag/k8s.io/cluster-autoscaler/enabled"
            values   = ["true"]
        }
    }
}

# Get the account ID, required for the helm chart below
data "aws_caller_identity" "current" {}

resource "helm_release" "k8s_autoscaler" {
    name       = "cluster-autoscaler"
    repository = "https://kubernetes.github.io/autoscaler"
    chart      = "cluster-autoscaler"
    version    = "9.7.0"
    namespace  = "kube-system"
    depends_on = [aws_iam_policy.cluster_autoscaler, module.iam_assumable_role_admin]

    # Configure the helm chart:
    set {
        name  = "awsRegion"
        value = var.aws_region
    }
    set {
        name  = "rbac.create"
        value = true
    }
    set {
        name  = "rbac.serviceAccount.name"
        value = local.autoscaler_account_name
    }
    set {
        name  = "rbac.serviceAccount.annotations.eks\\.amazonaws\\.com/role-arn"
        value = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/cluster-autoscaler"
    }
    set {
        name  = "autoDiscovery.clusterName"
        value = var.cluster_name
    }
    set {
        name  = "autoDiscovery.enabled"
        value = true
    }
}

####################################################################################################
## Kubernetes Dashboard (and metrics server for autoscaling)
####################################################################################################

resource "helm_release" "k8s_dashboard" {
    name       = "kubernetes-dashboard"
    repository = "https://kubernetes.github.io/dashboard"
    chart      = "kubernetes-dashboard"
    version    = "4.0.2"
    namespace  = "kube-system"

    # AWS EKS doesn't include the required "metrics-server" by default, so we need to install it too:
    set {
        name  = "metrics-server.enabled"
        value = "true"
    }
    # Name of the role (user) that we use to log in to the dashboard
    set {
        name  = "serviceAccount.name"
        value = "eks-admin"
        type  = "string"
    }
    # Give the default role (user) for the dashboard read-only permission.
    # "The basic idea of the clusterReadOnlyRole is not to hide all the secrets and sensitive data but more
    # to avoid accidental changes in the cluster outside the standard CI/CD."
    # Note that if this is set to false, the result is no permissions at all; for full permissions, they
    # must be created separately from the Helm chart as described at
    # https://docs.aws.amazon.com/eks/latest/userguide/dashboard-tutorial.html
    set {
        name  = "rbac.clusterReadOnlyRole"
        value = "true"
    }
}

####################################################################################################
## An S3 bucket to store the Tutor "state" (env folders)
####################################################################################################

resource "aws_s3_bucket" "tutor_env" {
    # Tutor isn't really designed for this use case, and it expects to be able to read/write some
    # intermediate files in the "env" subfolder of each instance's config folder.
    # To make this work on every developer's machine as well as on GitLab CI, we use an S3 bucket
    # to store the "env" folders for each instance and sync them after each tutor command.


    # Since bucket names must be globally unique, Terraform will automatically generate a random name for us.
    # cluster_name can contain underscores but that's not allowed for S3 names, so swap those out:
    bucket_prefix = "tutor-env-${replace(var.cluster_name, "_", "-")}-"
    # This holds credentials so is definitely private:
    acl           = "private"

    tags = {
        Name      = "Tutor State Bucket"
        ManagedBy = "Tutorraform"
        Terraform = "true"
    }
}

output "tutor_env_bucket_name" {
    value     = aws_s3_bucket.tutor_env.id  # This is the full bucket name, including prefix plus random suffix
    sensitive = false
}
