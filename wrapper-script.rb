#!/usr/bin/env ruby
require 'yaml'
require 'json'
require 'digest'

# This wrapper script helps run Terraform or kbuectl locally, ensuring that the correct version of Terraform/kubectl
# is used, the GitLab Terraform remote state backend is configured, and so on.
#
# Why is it written in Ruby?!?!?!
# Because Ruby seems to be the only scripting language installed on Mac and Linux by default
# that includes a YAML parser in its standard library :/


########################################################################################################################
# Make sure the user is running this script from the correct folder
########################################################################################################################

if $PROGRAM_NAME == "./tf"
    app = "terraform"
elsif $PROGRAM_NAME == "./kubectl"
    app = "kubectl"
elsif $PROGRAM_NAME == "./tutor"
    app = "tutor"
else
    raise "Unexpected program name #{$PROGRAM_NAME}\n"\
          "This should be run as ./tf or ./kubectl or ./tutor from the 'control' folder of a project that was "\
          "forked from https://gitlab.com/opencraft/dev/tutorraform-template/ . "\
          "It should look like:   cd my-cluster/control/ && ./tf init"
end

cluster_vars_file = "../cluster.yml"
if !(File.file?(cluster_vars_file))
    raise "Can't find #{cluster_vars_file}. \n"\
          "This should be run as ./tf or ./kubectl from the 'control' folder of a project that was "\
          "forked from https://gitlab.com/opencraft/dev/tutorraform-template/ . "\
          "It should look like:   cd my-cluster/control/ && ./tf init"
end

private_vars_file = "../private.yml"
if !(File.file?(private_vars_file))
    raise "To use this script locally, you need to set up some secrets in ../private.yml"
end

########################################################################################################################
# Load and compute required environment variables, found in various YAML files
########################################################################################################################

cluster_vars = YAML.load(File.read(cluster_vars_file))["variables"]
private_vars = YAML.load(File.read(private_vars_file))["variables"]
env_vars = cluster_vars.merge(private_vars)
env_vars = env_vars.transform_values {|v| v.to_s }  # Cast to string since YAML may have given us some integer values

provider = cluster_vars["TF_VAR_cluster_provider"]
tf_root = "../tutorraform/provider-#{provider}"

tf_main = "#{tf_root}/main.tf"
if !(File.file?(tf_main))
    raise "Cannot find any Terraform file #{tf_main}. Is TF_VAR_cluster_provider (#{provider}) in cluster.yml valid? Try 'aws'."
end

########################################################################################################################
# Helper functions
########################################################################################################################

def check_docker_is_running()
    @docker_checked ||= false
    if @docker_checked
        return
    end
    # Helper function to check if Docker is running.
    system('docker stats --no-stream > /dev/null')
    if $? != 0
        raise "Docker is not running. We run Terraform and kubectl and aws-cli using Docker, for consistency and so "\
              "you don't have to install them on your system. Please start docker."
    end
    @docker_checked = true
end

def get_tools_image_tag()
    # Helper function to build a docker container with aws-cli, kubectl, and tutor
    dockerfile_hash = Digest::SHA256.hexdigest File.read "../tutorraform/tools-container/Dockerfile"
    dockerfile_hash = dockerfile_hash[0..8]
    tag = "tutorraform-tools:#{dockerfile_hash}"
    # Check if the image exists:
    system("docker image ls #{tag}|grep tutorraform > /dev/null")
    if $? != 0
        # Build the image
        print "\nBuilding Docker tools container image\n\n"
        system("docker build --no-cache -t #{tag} ../tutorraform/tools-container")
        if $? != 0
            raise "Failed to build #{tag}"
        end
    end
    return tag
end

def get_kubeconfig_path()
    # Generate the kubeconfig-private.yml file if it's not present, so we can authenticate with kubernetes
    kubeconfig_file = "../kubeconfig-private.yml"
    if !(File.file?(kubeconfig_file))
        print "Generating required #{kubeconfig_file} using Terraform...\n"
        result = system("./tf output -raw kubeconfig > #{kubeconfig_file}")
        if $? != 0
            # This can fail if Terraform isn't initialized, in which case the kubeconfig file
            # Would just contain a Terraform error message.
            File.delete(kubeconfig_file) if File.exist?(kubeconfig_file)
            raise "Failed to create kubeconfig file. You probably need to run './tf init' first."
        end
        if !(File.file?(kubeconfig_file))
            raise "Failed to generate kubeconfig"
        end
    end
    return kubeconfig_file
end

def run_tool(tool_name, args, env_vars, mount_workspace, extra_mounts = [], extra_args = [])
    # Run a tool using our docker tools container (which contains AWS, kubectl, Tutor, etc.)

    # Check if Docker is running.
    check_docker_is_running()
    # Build the image we need, if necessary
    tools_tag = get_tools_image_tag()  # TODO: just host this on Docker Hub?

    docker_args = ["run", "-i", "-t", "--rm", "--workdir", "/workspace"]
    if mount_workspace
        docker_args.push("--mount", "type=bind,source=#{mount_workspace},destination=/workspace")
    end
    extra_mounts.each do |host_path, container_path|
        docker_args.push("--mount", "type=bind,source=#{File.expand_path(host_path)},destination=#{container_path}")
    end
    # Tell docker to pass through selected environment variables into the container:
    env_vars.each do |key, value|
        docker_args.push("-e", key)
    end
    docker_args.push(*extra_args, tools_tag, tool_name, *args)
    #system(env_vars, "echo", "docker", *docker_args)  # Uncomment this to see the command being run
    system(env_vars, "docker", *docker_args)
end

########################################################################################################################
# Terraform wrapper
########################################################################################################################
if app == "terraform"

    # Detect the Terraform version
    gitlab_ci_file = "../tutorraform/gitlab-ci/main.yml"
    gitlab_vars = YAML.load(File.read(gitlab_ci_file))["variables"]
    TERRAFORM_VERSION = gitlab_vars["TERRAFORM_VERSION_FULL"]

    env_vars["TF_STATE_NAME"] = "tf-state-#{provider}"

    # Set some derived values
    if !env_vars["GITLAB_PROJECT_NUMERIC_ID"]
        raise "You need to set GITLAB_PROJECT_NUMERIC_ID in private.yml's variables section."
    end
    # Configuration required to connect to the GitLab Terraform state backend:
    env_vars["TF_HTTP_ADDRESS"] = "https://gitlab.com/api/v4/projects/#{env_vars["GITLAB_PROJECT_NUMERIC_ID"]}/terraform/state/#{env_vars["TF_STATE_NAME"]}"
    env_vars["TF_HTTP_LOCK_ADDRESS"] = "https://gitlab.com/api/v4/projects/#{env_vars["GITLAB_PROJECT_NUMERIC_ID"]}/terraform/state/#{env_vars["TF_STATE_NAME"]}/lock"
    env_vars["TF_HTTP_UNLOCK_ADDRESS"] = env_vars["TF_HTTP_LOCK_ADDRESS"]

    # Check if Docker is running.
    check_docker_is_running()

    # Build a docker command to run Terraform, passing through required environment variables and command-line arguments
    docker_args = ["run", "-i", "-t", "--rm", "--workdir", "/workspace", "--mount", "type=bind,source=#{File.expand_path(tf_root)},destination=/workspace"]
    # Tell docker to pass through selected environment variables into the container:
    env_vars.each do |key, value|
        docker_args.push("-e", key)
    end
    docker_args.push("hashicorp/terraform:#{TERRAFORM_VERSION}")
    docker_args.push(*ARGV)  # Pass through all arguments to Terraform

    # Run Terraform!
    #system(env_vars, "echo", "docker", *docker_args)  # Uncomment this to see the command being run
    exec(env_vars, "docker", *docker_args)

########################################################################################################################
# kubectl wrapper
########################################################################################################################
elsif app == "kubectl"
    # Generate the kubeconfig-private.yml file if it's not present, so we can authenticate
    kubeconfig_file = get_kubeconfig_path()

    run_tool(
        "kubectl", ARGV,
        env_vars,
        mount_workspace=nil,
        extra_mounts=[
            [kubeconfig_file, "/root/.kube/config"]
        ],
        extra_args = ["--publish", "127.0.0.1:8001:8001"],  # Expose the port used by kubectl proxy for dashboard etc.
    )
elsif app == "tutor"
    instances_root = "../instances"
    available_instances = []
    Dir.chdir(instances_root) do
        available_instances = Dir.glob('*').select { |f| File.directory? f }
    end
    if available_instances.length() == 0
        # TODO: handle creating new instances, etc.
        raise "No instances available in the instances/ folder - did you copy the template, with at least an 'example' instance?"
    end
    if ARGV.length() < 1
        raise "\nUsage: ./tutor [instance_name] [tutor commands/arguments]\nWhere [instance_name] is one of: #{available_instances.join(', ')}"
    end

    instance_name = ARGV[0]
    instance_dir = File.expand_path("#{instances_root}/#{instance_name}")
    if !(File.directory?(instance_dir))
        raise "\nInvalid instance name - could not find #{instance_dir}"
    end

    # Generate the kubeconfig-private.yml file if it's not present, so we can authenticate
    kubeconfig_file = get_kubeconfig_path()

    # I had an approach where config.yml was split into config.yml (public) and private.yml (private)
    # But it was too annoying to maintain, since Tutor doesn't expect that and likes to rewrite its
    # config file, constantly putting the private values back in:
    ####### # To configure tutor, we pass the (public) config.yml folder as-is, but the private variables via environment vars:
    ####### instance_private_vars = YAML.load(File.read("#{instance_dir}/secrets.yml"))
    ####### # Prefix the keys with "TUTOR_" to make them environment variables, and format them as JSON
    ####### # We use JSON because .to_yaml puts the "---\n" header, and JSON is a subset of YAML, so works just as well.
    ####### env_vars = env_vars.merge(instance_private_vars.map {|k, v| ["TUTOR_#{k}", v.to_json] }.to_h)
    
    env_vars["TUTOR_ROOT"] = "/workspace"
    # Each separate instance gets deployed into a separate Kubernetes namespace:
    env_vars["TUTOR_ID"] = instance_name
    env_vars["TUTOR_K8S_NAMESPACE"] = instance_name

    # Make sure that the ID and namespace match what's expected:
    tutor_config_file = "#{instance_dir}/config.yml"
    if File.file?(tutor_config_file)
        instance_config = YAML.load(File.read(tutor_config_file))
        if instance_config.fetch("ID", instance_name) != instance_name || instance_config.fetch("K8S_NAMESPACE", instance_name) != instance_name
            raise "Consistency check failed: you should leave ID and K8S_NAMESPACE out of the config file, but if they're present they must match the instance name (#{instance_name})."
        end
    end

    # Pull the latest "env" folder for this instance from the shared storage (S3)

    bucket_name = `./tf output -raw tutor_env_bucket_name`
    remote_env_url = "s3://#{bucket_name}/#{instance_name}/env"
    s3_endpointargs = []
    if provider === "digitalocean"
        # We need to do some extra work to get the "aws s3" command line tool to work with DigitalOcean Spaces
        env_vars["AWS_ACCESS_KEY_ID"] = env_vars["SPACES_ACCESS_KEY_ID"]
        env_vars["AWS_SECRET_ACCESS_KEY"] = env_vars["SPACES_SECRET_ACCESS_KEY"]
        s3_endpointargs = ["--endpoint=https://#{env_vars["TF_VAR_do_region"]}.digitaloceanspaces.com"]
    end

    puts "Pulling latest env, if any..."
    run_tool(
        "aws", ["s3", "sync", remote_env_url, "/workspace/env", *s3_endpointargs],
        env_vars,
        mount_workspace=instance_dir,
    )
    if $? != 0
        raise "Failed to sync tutor 'env' folder with remote store."
    end

    run_tool(
        "tutor", ARGV.drop(1),  # Pass through all arguments except the instance_name
        env_vars,
        mount_workspace=instance_dir,
        extra_mounts=[
            [kubeconfig_file, "/root/.kube/config"]
        ],
    )

    puts "Syncing env to remote store..."
    run_tool(
        "aws", ["s3", "sync", "/workspace/env", remote_env_url, *s3_endpointargs],
        env_vars,
        mount_workspace=instance_dir,
    )
    if $? != 0
        raise "Failed to sync tutor 'env' folder with remote store."
    end
else
    raise "Unknown app"
end
