#!/usr/bin/env ruby
require 'yaml'

# This script combines 'main.yml' (Provision a cluster using Terraform on GitLab CI)
# with a dynamically generated CI stage that can run Tutor for every Open edX instance
# on this cluster.

instances_root = "./instances"
available_instances = []
Dir.chdir(instances_root) do
    available_instances = Dir.glob('*').select { |f| File.directory? f }
end

# Read "main.yml" as a string.
main_yml_str = File.read("tutorraform/gitlab-ci/main.yml")

additional_yml_str = "\n\n"
available_instances.each do |instance_name|
    additional_yml_str += <<~END_YML
        #{instance_name}:
          extends: .openedx_common
          variables:
            TUTOR_ROOT: ${CI_PROJECT_DIR}/instances/#{instance_name}
            TUTOR_ID: #{instance_name}
            TUTOR_K8S_NAMESPACE: #{instance_name}
    END_YML
    additional_yml_str += "\n\n"
end

File.write("tutorraform/gitlab-ci/generated.yml", main_yml_str + additional_yml_str)
