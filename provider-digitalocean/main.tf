terraform {
    # Use GitLab Terraform backend.
    backend "http" {
        # The following variables will be set automatically on GitLab CI thanks to the GitLab Terraform image
        # (see https://gitlab.com/gitlab-org/terraform-images/-/blob/master/src/bin/gitlab-terraform.sh)
        # If you want to run Terraform locally from your computer, you'll need to export these variables manually:
        #     TF_HTTP_ADDRESS, TF_HTTP_LOCK_ADDRESS, TF_HTTP_UNLOCK_ADDRESS, TF_HTTP_USERNAME, TF_HTTP_PASSWORD
        # Note that CI_PROJECT_ID is the *numeric* project ID shown on the project's GitLab homepage; it is not the project name/path.

        # address = "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/terraform/state/${TF_STATE_NAME}"
        # lock_address = "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/terraform/state/${TF_STATE_NAME}/lock"
        # unlock_address = "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/terraform/state/${TF_STATE_NAME}/lock"
        lock_method = "POST"
        unlock_method = "DELETE"
        retry_wait_min = 5
    }

    required_providers {
        digitalocean = {
            source = "digitalocean/digitalocean"
            version = "2.6.0"
        }
        # kubernetes = {
        #     source  = "hashicorp/kubernetes"
        #     version = "~> 2.0.2"
        # }
    }
}

variable "cluster_name" { type = string }
variable "cluster_max_node_count" { default = 5 }
variable "do_region" { default = "sfo3" }

# Pre-declare data sources that we can use to get the cluster ID and auth info, once it's created
# data "digitalocean_kubernetes_cluster" "cluster" {
#     name = var.cluster_name
# }

# If needed, we could configure the Kubernetes provider, and Helm
# provider "kubernetes" {
#     host                   = data.digitalocean_kubernetes_cluster.cluster.endpoint
#     token                  = data.digitalocean_kubernetes_cluster.cluster.kube_config[0].token
#     cluster_ca_certificate = base64decode(data.digitalocean_kubernetes_cluster.cluster.kube_config[0].cluster_ca_certificate)
# }

# provider "helm" {
#     kubernetes {
#         load_config_file       = false
#         host                   = data.digitalocean_kubernetes_cluster.cluster.endpoint
#         token                  = data.digitalocean_kubernetes_cluster.cluster.kube_config[0].token
#         cluster_ca_certificate = base64decode(data.digitalocean_kubernetes_cluster.cluster.kube_config[0].cluster_ca_certificate)
#     }
# }

####################################################################################################
## VPC: A virtual private cloud which will hold everything else
####################################################################################################

resource "digitalocean_vpc" "main_vpc" {
    name     = "${var.cluster_name}-vpc"
    region   = var.do_region
    ip_range = "10.10.10.0/24"
}

####################################################################################################
## Security Groups
####################################################################################################

# TODO: Allow SSH into the worker nodes from any other host in the VPC (such as a bastion)

####################################################################################################
## The Kubernetes Cluster itself
####################################################################################################

# This _must_ be in a separate module in order for us to use the credentials it provides
# to then deploy additional resources onto the cluster.
# https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/kubernetes_cluster#kubernetes-terraform-provider-example
# "When using interpolation to pass credentials from a digitalocean_kubernetes_cluster resource to the Kubernetes provider, the cluster resource generally should not be created in the same Terraform module where Kubernetes provider resources are also used. "

module "k8s_cluster" {
    source = "./k8s-cluster"

    cluster_name           = var.cluster_name
    cluster_max_node_count = var.cluster_max_node_count
    do_region              = var.do_region
    vpc_uuid               = digitalocean_vpc.main_vpc.id
}

# Declare the kubeconfig as an output - access it anytime with "/tf output -raw kubeconfig"
output "kubeconfig" {
    value     = module.k8s_cluster.kubeconfig.raw_config
    sensitive = true
}


####################################################################################################
## An S3-like bucket to store the Tutor "state" (env folders)
####################################################################################################

resource "random_id" "bucket_suffix" {
    byte_length = 8
}

locals {
    env_bucket_name = "tutor-env-${replace(var.cluster_name, "_", "-")}-${random_id.bucket_suffix.dec}"
}

resource "digitalocean_spaces_bucket" "tutor_env" {
    # Tutor isn't really designed for this use case, and it expects to be able to read/write some
    # intermediate files in the "env" subfolder of each instance's config folder.
    # To make this work on every developer's machine as well as on GitLab CI, we use an S3 bucket
    # to store the "env" folders for each instance and sync them after each tutor command.
    name   = local.env_bucket_name
    region = var.do_region
    acl    = "private" # This holds credentials so is definitely private.
}

output "tutor_env_bucket_name" {
    value     = local.env_bucket_name
    sensitive = false
}

####################################################################################################
## Project: A DigitalOcean Project to keep all resources organized together
####################################################################################################

resource "digitalocean_project" "project" {
    name        = var.cluster_name
    description = "Open edX deployment using Tutorraform"
    purpose     = "Web Application"
    environment = "Production"
}

resource "digitalocean_project_resources" "resources" {
    project = digitalocean_project.project.id
    resources = [
        # Workaround to attach a k8s cluster to a "project" - https://www.digitalocean.com/community/questions/attach-kubernetes-cluster-to-project
        "do:kubernetes:${module.k8s_cluster.cluster_id}",
        digitalocean_spaces_bucket.tutor_env.urn,
    ]
}
