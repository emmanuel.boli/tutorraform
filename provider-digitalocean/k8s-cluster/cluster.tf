terraform {
    # Use GitLab Terraform backend.
    backend "http" {
        # The following variables will be set automatically on GitLab CI thanks to the GitLab Terraform image
        # (see https://gitlab.com/gitlab-org/terraform-images/-/blob/master/src/bin/gitlab-terraform.sh)
        # If you want to run Terraform locally from your computer, you'll need to export these variables manually:
        #     TF_HTTP_ADDRESS, TF_HTTP_LOCK_ADDRESS, TF_HTTP_UNLOCK_ADDRESS, TF_HTTP_USERNAME, TF_HTTP_PASSWORD
        # Note that CI_PROJECT_ID is the *numeric* project ID shown on the project's GitLab homepage; it is not the project name/path.

        # address = "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/terraform/state/${TF_STATE_NAME}"
        # lock_address = "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/terraform/state/${TF_STATE_NAME}/lock"
        # unlock_address = "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/terraform/state/${TF_STATE_NAME}/lock"
        lock_method = "POST"
        unlock_method = "DELETE"
        retry_wait_min = 5
    }

    required_providers {
        digitalocean = {
            source = "digitalocean/digitalocean"
            version = "2.6.0"
        }
        kubernetes = {
            source  = "hashicorp/kubernetes"
            version = "~> 2.0.2"
        }
    }
}

variable "cluster_name" { type = string }
variable "cluster_max_node_count" { type = number }
variable "do_region" { type = string }
variable "vpc_uuid" { type = string }

data "digitalocean_kubernetes_versions" "available_versions" {}

resource "digitalocean_kubernetes_cluster" "cluster" {
    name         = var.cluster_name
    region       = var.do_region
    version      = data.digitalocean_kubernetes_versions.available_versions.latest_version
    vpc_uuid     = var.vpc_uuid
    auto_upgrade = true
    # "Surge upgrade makes cluster upgrades fast and reliable by bringing up new nodes before destroying the outdated nodes."
    surge_upgrade = true

    node_pool {
        name       = "${var.cluster_name}-workers"
        # Size options: s-1vcpu-2gb, s-2vcpu-2gb, s-2vcpu-4gb, s-4vcpu-8gb
        size       = "s-2vcpu-4gb"  # Tutor recommends at least 4GB per worker node.
        min_nodes  = 2
        max_nodes  = var.cluster_max_node_count
        auto_scale = true
    }
}

output "kubeconfig" {
    value     = digitalocean_kubernetes_cluster.cluster.kube_config[0]
    sensitive = true
}

output "cluster_id" {
    value = digitalocean_kubernetes_cluster.cluster.id
}
