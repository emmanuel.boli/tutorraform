# Tutorraform

This repository contains **GitLab CI scripts** that make it easy to deploy Open edX via [Tutor](https://docs.tutor.overhang.io/index.html) onto Kubernetes clusters at a provider like AWS. This is not an official part of Tutor nor Open edX; it is developed for the community by [OpenCraft](https://opencraft.com/).

What you get: Open edX + Tutor + GitLab CI + Terraform + Kubernetes

## Why do you want this?

- Before you can use Tutor in production, you need a Kubernetes cluster, continuous integration setup, and things like DNS entries. You may also want to use RDS as a database or store files on S3 instead of letting Tutor deploy those as Kubernetes services.
- You know better than to manually provision resources like that - it should be defined in code! That's what this does.
- Tutor seems to be designed to be used by a single person or run from a shared "admin host"; this repo provides infrastructure and best practices for sharing Tutor configuration/state among a team.
- In the future, this will allow using GitLab CI to run updates on (dozens of) Open edX instances in parallel, rather than manually running Tutor commands for each instance one at a time.

## Best practices that this repo uses

* Usage of this repo is split into two parts: the public, shared code (Terraform and GitLab CI scripts) which is in this repo, and [a private part](https://gitlab.com/opencraft/dev/tutorraform-template/) which contains deployment-specific configuration.
   * To use it, create a (private) fork from [the private template](https://gitlab.com/opencraft/dev/tutorraform-template/), which includes this public repo as a submodule. That way, everyone can keep their configuration private and versioned in git, while still collaborating on the development of the common code.
* All infrastructure is managed by Terraform.
* Terraform generally gets run by GitLab CI, and Terraform state is stored using GitLab's [managed Terraform state backend](https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html), which has a lot of nice features.
* However, you can also run Terraform from your computer for advanced use cases or operations like deleting/importing resources. This uses a wrapper script with best practices like running the correct version of Terraform (via Docker), and setting the variables for consistency with GitLab CI.
* **Secrets** related to the AWS account and the cluster as a whole are not stored in Git, but rather as GitLab CI variables. To use the repo locally, you need to put them into your private fork's `private.yml`, which is ignored by git. Secrets for each Open edX instance _are_ stored in git, because Tutor puts them into the config file and doing anything else is currently a pain (see TODOs below).

## Status

### Provider: AWS

Fully supported. Warning: even a small cluster with only two nodes (t3.medium) can cost you approximately US$200/month 😱. This may become more cost effective as you scale up to run multiple instances of Open edX on the same cluster.

### Provider: DigitalOcean

Fully supported. Approximate cost for a small cluster: $50/month.

### General TODOs

* Tutor is only run locally, not via CI. The current CI scripts show a "placeholder" job for each instance which is how Tutor _could_ be run via CI in the future (each parallel job would essentially run `./tutor NAME k8s quickstart --non-interactive` for one instance, when manually triggered to do so). This work needs to be completed so that GitLab CI can actually run Tutor.
* Some key Tutor commands rewrite `config.yml`, which is annoying - re-shuffling it and reformatting it, and adding keys like `ID` and `K8S_NAMESPACE` that we prefer to set by environment variables only. We should push an upstream fix to separate out some of the functions of `k8s quickstart` so we can deploy and update instances without Tutor making unwanted changes to the config file.
* Each instance has an `env` folder which is auto-generated by Tutor based on the `config.yml` file. Since it's mostly auto-generated and contains secrets, we do not store it in git but rather a secure S3 bucket. However, some Open edX customizations require making changes to the files in the `env` folder, and such changes should be checked into version control. The `wrapper-script.rb` file should be updated so that there is an `env-overrides` folder which _is_ version controlled, and when running Tutor, it should alway overwrite files in `env` with the version from `env-overrides`.
* Tutor prints annoying messages that complain about being run as root, which is perfectly fine when we're running it inside a docker container. Fix upstream.
* This contains a Dockerfile used to build a "tools container" image containing Tutor, awscli, kubectl, etc. We should set up GitLab CI to publish that image to Docker Hub or the GitLab container registry, rather than making each person build it. Then it can be used in the GitLab CI scripts too.
* The way that GitLab CI runs Terraform (and eventually Tutor) is currently a bit different than the way it is run locally (one uses a wrapper script; the other doesn't). It would be good to make these more consistent.
* DNS records must be created manually. It would be nice to do these using Terraform, though it may not be possible in general due to different patterns being used and not always being able to create a hosted zone per instance, if Studio is not a subdomain of the LMS domain.
* Extend the Terraform script to register the Kubernetes cluster with GitLab, so GitLab is aware of it. That also allows an easy way to install Prometheus monitoring of the cluster, fully integrated with GitLab.
* It would be really nice if Tutor could store the instance's auto-generated random secrets just in `env` and not in `config.yml`; then, there would be no secrets in version control. Ideally, a simple `rm instances/name/env/secrets.yml` would also be enough to do secret rotation (re-generate all secrets).

### Shared hosting TODOs

Currently, we can supports multiple Tutor instances on a single cluster by setting the `K8S_NAMESPACE` to something unique for each instance.

But to conserve costs, we should share persistent/costly resources across multiple instances. To do this, we can disable some services run by Tutor, and move them out to the shared cluster terraform.

* Currently, this uses Tutor to deploy a separate load balancer / ingress controller for each Open edX instance. It would be much simpler (and more affordable!) to instead deploy a single load balancer + ingress controller (Caddy or Traefik) for the whole cluster, and have it route traffic and handle TLS certificates for each LMS instance based on domain name.
* Currently, this uses Tutor to deploy a separate MySQL/MongoDB database container for each Open edX instance. It would be better to deploy a managed database for the cluster, such as Amazon RDS.
* This needs a mechanism for backing up MongoDB and static files [and MySQL, if not using RDS].
* Currently, this provisons a single Spaces/S3 bucket for the cluster to store the Tutor `env` files. This needs to be changed to deploy namespaced buckets for each instance.
* Need to enable the [Tutor minio plugin](https://docs.tutor.overhang.io/k8s.html#s3-like-object-storage-with-minio) and configure this to use namespaced buckets for each instance's static/object storage.
* Tutor needs log rotation added, for monitoring and analytics tracking.
* Currently, so manual steps are required before asking Terraform to destroy an instance. These steps needs to be automated.


--------------

## How to use this

### Getting started

#### Part 1: Fork

1. [Create a fork](https://gitlab.com/opencraft/dev/tutorraform-template/-/forks/new) of [`tutorraform-template`](https://gitlab.com/opencraft/dev/tutorraform-template/) (note: it's different than this repo) within GitLab.
1. **Important:** When viewing your fork in GitLab, go to Settings > General > Visibility, and change the project visibility to **Private**.
1. When viewing your fork in GitLab, go to Settings > General > Advanced > Change Path, and rename the fork from `tutorraform-template` to whatever you like (`my-cluster` is the example name used in the rest of this README).

#### Part 2a: If you you want to use AWS

1. See the warning above about costs.
1. Edit `cluster.yml` and set the name of your cluster and any other settings you'd like. You can also update `README.md` to describe the purpose of your new cluster. Commit your changes.
1. Create an AWS account, or use an existing one.
1. Login to AWS and [create an IAM user](https://console.aws.amazon.com/iam/home) with user name `tutorraform` (or whatever you'd like), Access Type: "Programmatic access", attach existing policy directly: "AdministratorAccess". Note the access key and secret key for the next step.
1. In your private fork, go to Settings > CI / CD > Variables, and create the following variables:
   - `AWS_ACCESS_KEY_ID` (use the access key value given for the new IAM user - it should start with `AKIA`)
   - `AWS_SECRET_ACCESS_KEY` (use the secret key value given for the new IAM user)
   - It will be easiest if you set each variable to be "Masked" but not "Protected", however you can do what you feel is best.

#### Part 2b: If you you want to use DigitalOcean

1. Edit `cluster.yml` and set the name of your cluster and any other settings you'd like. Be sure to set `TF_VAR_cluster_provider` to `digitalocean`. You can also update `README.md` to describe the purpose of your new cluster. Commit your changes.
1. Create an DigitalOcean account, or use an existing one.
   Note that [DigitalOcean's UI doesn't play nice with AdBlock+](https://www.digitalocean.com/community/questions/can-t-access-to-do-with-firefox), so you may need to disable this browser plugin.
1. Login to the DigitalOcean control panel and go to [Account > API > Tokens](https://cloud.digitalocean.com/account/api/tokens). Create both a "personal access token" and a "Spaces access key". You can put "Tutorraform" or whatever you'd like for the name.
1. In your private fork, go to Settings > CI / CD > Variables, and create the following variables:
   - `DIGITALOCEAN_TOKEN` (use the secret part of your personal access token)
   - `SPACES_ACCESS_KEY_ID` (use the new access key ID)
   - `SPACES_SECRET_ACCESS_KEY` (use the new access key secret)
   - It will be easiest if you set each variable to be "Masked" but not "Protected", however you can do what you feel is best.

#### Part 3: Deploying the cluster

1. In your private fork on GitLab, go to "CI/CD" > Pipelines. Click the green "Run Pipeline" button. Select the `main` branch, and then press "Run Pipeline" again.
   -  On that new pipeline's status page, find the "Downstream" section and click on the child pipeline, once ready. It has three phases: init, plan, and apply. Wait for "init" and "plan" to successfully complete.
   - Now manually run the "Terraform apply" phase. This will deploy your new cluster! It will probably take 10-15 minutes.
1. Follow the steps for "How to add a new Open edX instance to the cluster" below to create your first Open edX instance on the cluster.

### Working with this locally

1. Make sure you have Ruby installed locally and that Docker is running. You do not need to have Tutor, Terraform, kubectl, awscli, or anything else installed, because they get run inside Docker - but Docker and Ruby are required to run the provisioning scripts.
1. Clone your private fork of [`tutorraform-template`](https://gitlab.com/opencraft/dev/tutorraform-template/) to your local computer. **In all the examples below, `my-cluster` refers to your private fork of `tutorraform-template`.**
1. Within your fork (`my-cluster`), run `git submodule init` and `git submodule update`.
1. Create `my-cluster/private.yml` based on `private.yml.example`, and set the required GitLab and cloud provider credentials. You only need `private.yml` for working from your local computer; it is not needed for using this with GitLab CI.
1. Change into the `control` directory, e.g. `cd my-cluster/control/` . All commands are run from this directory.
1. Run `./tf init` to initialize Terraform. Even if you are only planning to run other commands like Tutor or Kubernetes, it's necessary to do this first, since other commands depend on Terraform (for authentication).

### How to add a new Open edX instance to the cluster

1. Make sure you are set up to work with this locally (see "Working with this locally").
1. Change into the `control` directory, e.g. `cd my-cluster/control/`
1. Run `mkdir ../instances/NAME` where `NAME` is the ID you want to use for the new instance
1. Run `./tutor NAME config save --interactive` where `NAME` is the ID of the new instance. Be sure to choose "y" for "Are you configuring a production platform?" and "Activate SSL/TLS"
1. Run `./tutor NAME k8s quickstart --non-interactive` to deploy the new instance onto your cluster.
1. Commit your changes using git to save the new `config.yml`. (It contains sensitive values, so your repo better be private!)
1. Log in to your cloud provider's control panel (AWS or DigitalOcean, etc.), and go to the "load balancers" page. Find the IP or hostname of the load balancer, and set up the required DNS records to point to it (your LMS domain, `preview.[lms domain]`, and your Studio domain all need to point to the load balancer). Currently it is different for each instance, but we will soon fix that so it uses a consistent IP / load balancer for the whole cluster (much more affordable and simple).
1. Wait a few minutes, then try accessing the instance. If you get an "SSL protocol error", wait a bit longer - it takes some time until DNS records have propagated and Caddy configures the required HTTPS certificates.
1. Create an admin user with `./tutor NAME k8s createuser --staff --superuser Username email`

### How to update an Open edX instance to the cluster

Say you've made changes to a `config.yml`, or new Open edX images have been released upstream.

1. Change into the `control` directory, e.g. `cd my-cluster/control/`
1. Run `./tutor NAME k8s quickstart --non-interactive` to update the instance with ID `NAME`.

### How to run Terraform from your local computer

Normally you should let GitLab CI run Terraform, but if you need to do something complex or you want to destroy some resources, you'll need to run custom Terraform commands.

1. Make sure you are set up to work with this locally (see "Working with this locally").
1. Open a terminal in the `control` directory, e.g. `cd my-cluster/control/`
1. Run Terraform as `./tf`, for example `./tf plan` will show any changes that aren't yet applied.

### How to run Kubernetes maintenance commands from your local computer

You can use `kubectl` from your local computer. You don't need to install it, nor `aws-cli` - just Docker.

1. Make sure you are set up to work with this locally (see "Working with this locally").
1. Change into the `control` directory, e.g. `cd my-cluster/control/`
1. Make sure Terraform is working (see above), i.e. that `./tf refresh` works without error.
1. Run commands using `./kubectl`, like this: `./kubectl get pods --all-namespaces`

Note: Some network-related `kubectl` commands may not work as expected, since we run `kubectl` inside a docker container. Specifically, by default kubectl can only forward network traffic using port 8001 on your host, and kubectl must be listening on 0.0.0.0. See "How to access the Kubernetes Dashboard" for AWS below for an example.

More useful commands:

* Launch a shell on the first `lms` pod:

   ```
   ./kubectl exec -it $(./kubectl get pods --namespace=<NAME> -l app.kubernetes.io/name=lms -o jsonpath='{range .items[0]}{@.metadata.name}{end}') -- bash -c 'DJANGO_SETTINGS_MODULE=$SERVICE_VARIANT.envs.$SETTINGS bash -l'
   ```
* Tail the logs from the `lms` pod:

   ```
   ./kubectl logs -f $(./kubectl get pods --namespace=<NAME> -l app.kubernetes.io/name=lms -o jsonpath='{range .items[*]}{@.metadata.name}{end}')
   ```


### How to run Tutor commands from your local computer

You can run `tutor` commands directly from your local computer. You don't need to install it - just Docker.

1. Make sure you are set up to work with this locally (see "Working with this locally").
1. Change into the `control` directory, e.g. `cd my-cluster/control/`
1. Run commands using `./tutor instance_name [command...]`, like this: `./tutor site1 k8s quickstart --non-interactive`

### How to access the Kubernetes Dashboard

DigitalOcean: Just log in to your DigitalOcean dashboard, click Clusters > (the cluster) > Dashboard.

AWS:

1. Make sure you are set up to work with this locally (see "Working with this locally").
1. Change into the `control` directory, e.g. `cd my-cluster/control/`
1. Run `./kubectl -n kube-system describe secret eks-admin-token | grep token:` and copy the token value.
1. Run `./kubectl proxy --address='0.0.0.0'` to start the proxy, which opens a tunnel that allows you to connect to services running on the cluster.
1. Go to http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:https/proxy/ and enter the (very long) token that the earlier command printed.


### How to tear everything down

If you've just been testing this out and you want to shut it down to avoid further charges from your cloud provider, it's really easy to destroy everything. Obviously, only run this if you're sure you want to destroy everything - this cannot be undone!

If you are using AWS, you'll first need to log in to the console:
1. Find the S3 bucket called "tutor-env-...." and delete all of its files. (Terraform can't delete S3 buckets if they contain files.)
1. Go to EC2 load balancers, and delete any load balancers pointing into the Kubernetes cluster.

If you're using DigitalOcean, you'll first need to login to the UI:
1. Go to Spaces, find bucket created for your project, and delete the top-level folder. (Terraform can't delete Spaces buckets if they contain files.)
1. Go to Networking > VPC, and find the VPC created for your project.

   Click on Resources and delete the load balancer attached to the VPC. (Terraform cannot delete the VPC if it has members.)

   If the VPC is marked as "default", create a new VPC and make that the default. (Terraform cannot delete the default VPC.)

Then:
1. Make sure you are set up to work with this locally (see "Working with this locally").
1. Change into the `control` directory, e.g. `cd my-cluster/control/`
1. Run `./tf destroy` . It may take quite a long time.

### Troubleshooting

Tips on how to deal with some common errors.

#### control characters are not allowed

Problem:

This error can occur if you try to run `./tutor NAME k8s quickstart` before terraform has finished applying your changes and creating your cluster.

```
error: error loading config file "/root/.kube/config": yaml: control characters are not allowed
Error: Command failed with status 1: kubectl apply --kustomize /workspace/env --wait --selector app.kubernetes.io/component=namespace
```

Solution:

```
# Let the wrapper script regenerate the config file
cd my-cluster/control/
rm ../kubeconfig-private.yml
```
#### Provisioning the first Open edX instance fails (get_kubeconfig_path fails)

Problem:

This error can occur when you run `/tutor NAME config save --interactive` when provisioning the first Open edX instance. The execution of `get_kubeconfig_path` will error out with an error related to filesystem permissions.

Solution:

Bypass the calls to `get_kubeconfig_path` and write the output manually using:

```
cd my-cluster/control/
./tf output -raw kubeconfig > ../kubeconfig-private.yml
```
